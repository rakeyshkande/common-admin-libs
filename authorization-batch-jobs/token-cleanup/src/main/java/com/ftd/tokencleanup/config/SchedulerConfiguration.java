/**
 * 
 */
package com.ftd.tokencleanup.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import com.ftd.tokencleanup.AccessTokenCleanupService;

/**
 * The Class SchedulerConfiguration.
 *
 * @author Varun Arora
 */
@Configuration
public class SchedulerConfiguration {
	
	/** The data source. */
	@Autowired
	private DataSource dataSource;
	
	/**
	 * Token store.
	 *
	 * @return the token store
	 */
	@Bean
	public TokenStore tokenStore() {
		return new JdbcTokenStore(dataSource);
	}
	
	@Bean AccessTokenCleanupService accessTokenCleanupService() {
		return new AccessTokenCleanupService();
	}
	
}
