package com.ftd.tokencleanup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Varun Arora
 *
 */
@SpringBootApplication
public class TokenCleanupApplication {
	

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(TokenCleanupApplication.class, args);
		AccessTokenCleanupService accessTokenCleanupService =ctx.getBean(AccessTokenCleanupService.class);
		accessTokenCleanupService.removeInvalidTokens();
		ctx.close();
	}
	
}
