package com.ftd.tokencleanup;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * The Class AccessTokenCleanupShedulerService..
 * this executes at 1 AM on every day 
 * AccessTokenCleanupService to remove expired tokens from table
 * @author Varun Arora
 */

@Service
public class AccessTokenCleanupService {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/** The token store. */
	@Autowired
	TokenStore tokenStore;

	/** The client ids.
	 * this reads values from configuration file 
	 */
	@Value("#{'${jwt.token.clientids}'.split(',')}")
	private List<String> clientIds;

	/** The tokens. */
	Collection<OAuth2AccessToken> tokens;

	/**
	 * Removes the invalid tokens.
	 * removeInvalidTokens() method executes at 1 AM on every day
	 * it removes expired tokens from table
	 */
	@Transactional
	public void removeInvalidTokens() {
		log.info("removeInvalidTokens method starts ");
		clientIds.forEach(clientId -> tokens = tokenStore.findTokensByClientId(clientId));
		if (null != tokens) {
			tokens.forEach(token -> {
				if (token.isExpired()) {
					tokenStore.removeAccessToken(token);
					log.info("removeInvalidTokens method: token removed from db: "+token +"time stamp:"+LocalDateTime.now());
				}
			});
		}
		log.info("removeInvalidTokens: method end ");

	}


}
